<?php
    class Modele
    {
        private $unPdo, $uneTable;

        public function __construct($server, $bdd, $user, $mdp)
        {
            $this->unPdo = null;
          
            try
            {
                $this->unPdo = new PDO("mysql:host=".$server.";dbname=".$bdd , $user, $mdp, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
            }
            catch(PDOException $exp)
            {
                echo "Erreur de connecxion : ".$exp->getMessage();
            }
        }

        //l'attribut uneTable -> peremt de generer n'importe quelle table , function generique qui permet de generer n'importe qu'elle table, onl'utilise dans la function selectAll
        public function getTable()
        {
            return $this->uneTable;
        }

        public function setTable($uneTable)
        {
            $this->uneTable = $uneTable;
        }

        public function selectAll()
        {
            $requete = "select * from ".$this->uneTable.";";
            $select = $this->unPdo->prepare($requete);
            $select->execute();
            return $select->fetchAll();
        }

        //Fonction pour insérer un élément dans une table
        public function insert($tab)
        {
            $champs = array();
            $donnes = array();
            foreach($tab as $cle => $valeur)
            {
                $champs[] = ":".$cle;
                $donnes[":".$cle] = $valeur;
            }
            $chaineChamps = implode(",", $champs);

            $requete = "insert into ".$this->uneTable." values (null,".$chaineChamps.");";
            //echo $requete ;
            $select = $this->unPdo->prepare($requete);
           // var_dump($donnes);
            $select->execute($donnes);
        }

        //Fonction pour supprimer un élément d'une table
        public function delete($where)
        {
            $champs = array();
            $donnes = array();

            foreach($where as $cle => $valuer)
            {
                $champs[] = $cle ." = :".$cle;
                $donnes[":".$cle] = $valuer;

            }
            $chaineWhere = implode(" and ", $champs);
            $requete = "delete from ".$this->uneTable." where ".$chaineWhere; //une requete générique pour n'importe quelle table
            
            $delete = $this->unPdo->prepare($requete);
            $delete->execute($donnes);
        }

        //Permet de recuperer n'importe quelle donnée en spécifiant la clause 'where'
        public function selectWhere($where)
        {
            $champs = array();
            $donnes = array();

            foreach($where as $cle => $valuer)
            {
                $champs[] = $cle ." = :".$cle;
                $donnes[":".$cle] = $valuer;

            }
            $chaineWhere = implode(" and ", $champs);
            $requete = "select * from ".$this->uneTable." where ".$chaineWhere; //une requete générique pour n'importe quelle table
            
            $select = $this->unPdo->prepare($requete);
            $select->execute($donnes);
            return $select->fetch(); // return un seul resultat avec fetch
        }

        //Permet de recuperer n'importe quelle donnée en spécifiant la clause 'where'
        public function selectWhereAll($where)
        {
            $champs = array();
            $donnes = array();

            foreach($where as $cle => $valuer)
            {
                $champs[] = $cle ." = :".$cle;
                $donnes[":".$cle] = $valuer;

            }
            $chaineWhere = implode(" and ", $champs);
            $requete = "select * from ".$this->uneTable." where ".$chaineWhere; //une requete générique pour n'importe quelle table
            
            $select = $this->unPdo->prepare($requete);
            $select->execute($donnes);
            return $select->fetchAll(); // return un seul resultat avec fetch
        }


        public function update($tab, $where)
        {   //
            //c'est pour chaineWhere
            $champs = array();
            $donnes = array();

            foreach($where as $cle => $valuer)
            {
                $champs[] = $cle ." = :".$cle;
                $donnes[":".$cle] = $valuer;

            }
            $chaineWhere = implode(" and ", $champs);
            


            // c'est pour chainechamps
            $champs2 = array();

            foreach($tab as $cle => $valuer)
            {
                $champs2[] = $cle ." = :".$cle;
                $donnes[":".$cle] = $valuer;
            }
            $chaineChamps = implode(", ", $champs2);

            // apres on prépare la requete

            $requete = "update ".$this->uneTable." set ".$chaineChamps." where ".$chaineWhere; //une requete générique pour n'importe quelle table
            $update = $this->unPdo->prepare($requete);
            $update->execute($donnes);
        }

        //Fonction qui permet de recherhcer un mot / chaine de caractère
        public function selectSearch($tab, $mot)
        {
            $champs = array();
            $donnees = array(":mot"=>"%".$mot."%"); //le porucentage pour dire que ca commmence par .. et finit par ...
            foreach($tab as $cle)
            {
                $champs[] = $cle ." like :mot";
            }
            $chaineWhere = implode(" or ", $champs);

            $requete = "select * from ".$this->uneTable." where ".$chaineWhere;
            $select = $this->unPdo->prepare($requete);
            $select->execute($donnees);
            return $select->fetchAll();
        }

        //Methode generique qui permet de déterminer le nombre d'enregistrement dans une table.
        public function count()
        {
            $requete = "select count(*) as nb from ".$this->uneTable;
            $select = $this->unPdo->prepare($requete);
            $select->execute();
            return $select->fetch();
        }

        //Fonction qui appele les procedures
        public function insertProcedure($procedure, $tab)
        {
            $champs = array(); 
            $donnees = array(); 
            foreach ($tab as $cle => $valeur) {
                 $champs[] = ":".$cle;
                 $donnees[":".$cle] = $valeur; 
            }
            $chaineChamps = implode(",", $champs); 

            $requete ="call ".$procedure." (".$chaineChamps.");";

            $select = $this->unPdo->prepare ($requete);
            $select->execute ($donnees);
        }

        //Fonction qui permet d'incrémenter le nombre de vote d'une réponse après un vote
        public function updateVote($id) {
            $req1 = $this->unPdo->prepare('UPDATE reponse SET nbReponse = nbReponse+1 WHERE idReponse = "'.$id.'"');
            $req1->execute(array($id));
            $req1->closeCursor();
        }
        
        // Trouver l'utilisateur à travers son mail
        public function findUserByEmail($email){
        $req = $this->unPdo->prepare('SELECT * FROM sondeur WHERE email = :email');
        // Bind value
        $req->bindParam(':email', $email, PDO::PARAM_STR);
        $req->rowCount();
        $req->execute();
        // Check row
        if($req->rowCount() > 0){
            return true;
        }else{
            return false;
        }
    }
}
?>