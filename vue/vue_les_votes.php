<?php
if(!isset($_SESSION['id'])) { //Si non connecté
	header('Location: index.php'); //Redirection dans la page principale
}

$id = $_GET['vote_sondage']; //On récupère l'id du sondage dans l'url
$idQ=$_GET['idQuestion'];

foreach ($lesVotes as $unVote) { //On parcours la vue des votes pour récupérer quelques variables (B1)
	if($unVote['idSondage'] == $id and $idQ == $unVote['idQuestion']) { //Si l'id su sondage affiché est bien celui récupéré dans l'url (C1)
		//Affectations des varaibles pour faciliter leurs appels
		$idQuestion = $unVote['idQuestion'];
		$question = $unVote['question'];
		$auteur = $unVote['nom'];
		$date = $unVote['dateCreation'];
		$etat = $unVote['etat'];
	} //(C1)
} //(B1)

////////////////////////////////////////////////
/* EMPECHE L'UTILISATEUR DE VOTER PLUSIEURS FOIS */
$var = "vote_".$_SESSION['id']."_question_".$idQuestion; //Instanciation du nom du cookie avec l'id de l'utilisateur et l'id de la question à laquelle on veut accèder
if(isset($_COOKIE[$var])) { //Si le cookie de vote existe (C4)
	echo '<p>Vous avez déjà voté. Vous ne pouvez pas repondre.</p>';
	echo '<p>Consulter les résultats <a href="index.php?resultat_sondage='.$id.'&idQuestion='.$idQ.'">ici</a></p>';
} else {//(C4)
////////////////////////////////////////////////

//Si le sondage est ouvert alors on affiche la suite
if($etat == "Ouvert") { //(C2)  //////////////////ET EST UNE PERSONNE
?>

<!-- Bloc qui permet d'afficher la date, la question et l'auteur du sondage -->
<p>Créé le <?= $date; ?></p>
<h5><?= $question; ?></h5>
<p>Par <?= $auteur; ?></p>
<br><br>

<form action="#" method="POST">
<?php 
foreach ($lesVotes as $unVote) { //On parcours la vue des votes pour récupérer quelques variables (B2)
	if($unVote['idSondage'] == $id and $idQ == $unVote['idQuestion']) { //Si l'id su sondage affiché est bien celui récupéré dans l'url (C3)
?>
	<label>
		<input type="radio" name="reponse" value="<?= $unVote['idReponse']; ?>" />
		<span><?= $unVote['reponse']; ?></span>
	</label>
	<br><br>

<?php
	} //(C3)
} //(B2)	
?>
	<br>
	<button class="btn waves-effect waves-light" type="submit" name="Repondre" value="Repondre">Voter</button>
</form>

<?php 
} else { //C2 
	//Messages si jamais on a réussi à accèder à la page des vote pour un sondage fermé :
	echo 'index.php?resultat_sondage='.$id.'&idQuestion='.$idQ.'';
	echo '<p>Le sondage est fermé. Vous ne pouvez pas repondre.</p>';
	echo '<p>Consulter les résultats <a href="index.php?resultat_sondages='.$id.'&idQuestion='.$idQ.'">ici</a></p>';
} //(C2)

} //(C4)

?>