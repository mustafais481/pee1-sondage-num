<div class="row" align="center">

<?php 
//On récupère l'id de la categorie que l'on a choisi (dans l'url)
$id = $_GET['idCategorie'];

foreach ($lesSousCategories as $uneSousCategorie) {
  if($id == $uneSousCategorie['idCategorie']) {
?>

    <div class="col l6 m6 s12">
    <div class="card">
    <div class="card-content">
      <?php
      $nb = 0; 
      foreach ($lesSondages as $unSondage) {
        if($unSondage['idSous_categorie'] == $uneSousCategorie['idSous_categorie']) {
          $nb++;
        }
      }
      ?>
      <span class="card-title activator grey-text text-darken-4"><?= $uneSousCategorie['nom'].' - Sondages : '.$nb; ?></span>
    </div>
    <div class="card-image waves-effect waves-block waves-light">
      <img class="activator" src="img/sous_categorie/<?= $uneSousCategorie['nom']; ?>.jpg" alt="<?= $uneSousCategorie['description']; ?>">
    </div>
    <div class="card-content">
      <p><a href="index.php?idSous_categorie=<?= $uneSousCategorie['idSous_categorie']; ?>">Voir les sondages de cette sous-catégorie</a></p>
    </div>
    <div class="card-reveal">
      	<span class="card-title grey-text text-darken-4"><?= $uneSousCategorie['nom']; ?><i class="material-icons right">close</i></span>
      	<p><?= $uneSousCategorie['description']; ?></p>
    </div>
  	</div>
    </div>

<?php 
  }
}
?>

</div>