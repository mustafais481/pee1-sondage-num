<?php

if(isset($_POST['inscription_personne']))
{

        $mdphash = hash('sha512', $_POST['mdp']);

        $tab = array("nom"=>$_POST['nom'], 
                    "email"=>$_POST['email'],
                    "tel"=>$_POST['tel'], 
                    "adresse"=>$_POST['adresse'], 
                    "mdp"=>$mdphash,
                    "role"=>'user',
                    "prenom"=>$_POST['prenom'], 
                    "age"=>$_POST['age']
                    );       

       if(!$unControleur->findUserByEmail($tab['email'])){
            $unControleur->insertProcedure("insertPersonne", $tab);
            header("Location: index.php?connexion");
        }
        echo '<script language="javascript">';
        echo 'alert("Ce compte est déja pris, veuillez en choisir un autre")';
        echo '</script>';
}

    require_once("vue/vue_insert_les_personnes.php");

?>