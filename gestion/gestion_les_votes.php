<p class="flow-text">Voter</p>
<br>

<?php 

//On récupère la vue sondage_affichage
$unControleur->setTable("vote_sondage"); 
$lesVotes = $unControleur->selectAll();

require_once("vue/vue_les_votes.php");

$var = "vote_".$_SESSION['id']."_question_".$idQuestion; //Nom du cookie comportant l'id de la question

if(isset($_POST["Repondre"])) {
	$idRep = strip_tags($_POST['reponse']);

	try {
		$update = $unControleur->updateVote($idRep);

		//Création du cookie
		setcookie(
			$var, //Nom du cookie
			$idRep, //Contenu du cookie = id de la question répondue
			[
				'expires' => time() + 365*24*3600, //Expiration du cookie (1 an ici)
				'secure' => true, //Sécurité (cookie inaccessible en JS)
				'httponly' => true, //Sécurité (cookie inaccessible en JS)
			]
		);

		header("Location: index.php?question_sondage=".$id);
	} catch (Exception $e) {
		echo "Problème avec la méthode modifCategorie (fichier : functions.php) : ";
	}

}

?>