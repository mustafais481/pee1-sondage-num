<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>SondageNum.com - Inscription</title>
</head>
<body>
<div id="container">
  <div id="left">
    <p id="texte">
      Bienvenue sur SondageNum,<br>
      Le site de sondage pour entreprises et particuliers,<br>
      créer par l'INSEE.
    </p>
  </div>
  
     <script>
            document.getElementById('see-button').addEventListener('click', evt => {
             document.getElementById('blur-work').style.display = 'none';
             })
     </script>

<?php

if (!empty($errors)) {
    foreach ($errors as $error){
        echo '<p>'.$error.'</p>';
    }
}
?>

    <div id="right">
    <h1 id="login">Inscription</h1><br><br>
   
    <p id="texte">
      Inscrivez vous sur SondageNum,<br>
      Ditês nous si vous êtes une entreprise ou un particulier.<br><br><br><br>
    </p>

<form method="post">

<button class="btn waves-effect waves-light" type="submit" name="Entreprise" value="Entreprise" >ENTREPRISE</button>
<button class="btn waves-effect waves-light" type="submit" name="Particulier" value="Particulier" >PARTICULIER</button>

    </div>
    </div>
</form>

 
 <?php

    if(isset($_POST['Particulier']))
    {
        header("Location: index.php?Particulier");
    } 

    if(isset($_POST['Entreprise']))
    {
        header("Location: index.php?Entreprise");
    } 
?>

                
</body>
</html>